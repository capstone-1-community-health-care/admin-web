import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
} from "react-router-dom";


import RootLayout from "./Layout/RootLayout";


import Login from "./pages/Login"
import Register from "./pages/Register";
import Home from "./pages/Home"
import Chat from "./pages/Chat";
import Faqs from "./pages/Faqs";
import Profile from "./pages/Profile";
import StudentData from "./pages/StudentData";

const router = createBrowserRouter(
  createRoutesFromElements (
    <Route path="/">
      <Route index element={<Login />} />
      <Route path="register" element={<Register />} />
      <Route path="user" element={<RootLayout />} >
        <Route path="home" element={<Home />} />
        <Route path="student data" element={<StudentData />} />
        <Route
          path="chat"
          element={<Chat />}
        />
        <Route path="faqs" element={<Faqs />} />
        <Route path="profile" element={<Profile />} />
      </Route>
    </Route>
  )
);

function App() {
  return <RouterProvider router={router} />;
}

export default App;