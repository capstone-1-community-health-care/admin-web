// FaqAccordion.js
import React, { useState } from 'react';
import faqData from '../assets/js/faqsdata'; 

import "../assets/css/faqs.css"

function Faqs() {
  const [expandedItems, setExpandedItems] = useState([]);

  const toggleItem = (index) => {
    if (expandedItems.includes(index)) {
      setExpandedItems(expandedItems.filter((item) => item !== index));
    } else {
      setExpandedItems([...expandedItems, index]);
    }
  };

  return (
    <div className="faqs-body" 
    style={{ backgroundImage: `url("/images/FAQs bg.png")` }}>
      <div className="accordion">
        <div className="image-box">
          <img src="/images/faqs.png" alt="" />
        </div>
        <div className="accordion-text">
          {/* <div className="title">FAQ</div> */}
          <ul className="faq-text">
            {faqData.map((item, index) => (
              <li
                key={index}
                className={expandedItems.includes(index) ? 'showAnswer' : ''}
                onClick={() => toggleItem(index)}
              >
                <div className="question-arrow">
                  <span className="question">{item.question}</span>
                  <img src="/svg/expand.svg" alt="" />
                  {/* <i className="bx bxs-chevron-down arrow"></i> */}
                </div>
                <p>{item.answer}</p>
                <span className="line"></span>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Faqs;
