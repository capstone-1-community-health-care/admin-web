import React, { useState, useEffect, useRef, useLayoutEffect } from 'react';
import { firestore, auth } from '../firebase';
import '../assets/css/Chat.css';
import emojiList from '../assets/js/emojiList';

const Chat = () => {
  const [users, setUsers] = useState([]);
  const [selectedConversation, setSelectedConversation] = useState(null);
  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');
  const [showEmojiPicker, setShowEmojiPicker] = useState(false);
  const messageListRef = useRef(null);
  const [selectedMessage, setSelectedMessage] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');

  const getConversationId = (uid1, uid2) => {
    return [uid1, uid2].sort().join('_');
  };

  useEffect(() => {
    const unsubscribeUsers = firestore.collection('users').onSnapshot((snapshot) => {
      const usersData = snapshot.docs
        .map((doc) => ({
          uid: doc.id,
          ...doc.data(),
        }))
        .filter((user) => user.uid !== auth.currentUser.uid);

      setUsers(usersData);
    });

    return () => {
      unsubscribeUsers();
    };
  }, []);

  useEffect(() => {
    if (selectedConversation) {
      const unsubscribeMessages = firestore
        .collection('conversations')
        .doc(selectedConversation)
        .collection('messages')
        .orderBy('timestamp', 'desc')
        .onSnapshot((snapshot) => {
          const messageData = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
          setMessages(messageData.reverse());
        });

      return () => unsubscribeMessages();
    }
  }, [selectedConversation]); 

  useLayoutEffect(() => {
    const messageList = messageListRef.current;

    const handleScrollToBottom = () => {
      if (messageList) {
        messageList.scrollTop = messageList.scrollHeight;
      }
    };

    handleScrollToBottom();
  }, [messages]);

  const handleUserClick = (otherUserId) => {
  try {
    const currentUserID = auth.currentUser.uid;
    const conversationID = getConversationId(currentUserID, otherUserId);
    setSelectedConversation(conversationID);

    // Listen for changes to the last message document for the current user
    const currentUserLastMessageRef = firestore.collection('conversations').doc(conversationID).collection('lastMessages').doc(currentUserID);
    const currentUserUnsubscribe = currentUserLastMessageRef.onSnapshot((currentUserLastMessageSnapshot) => {
      if (currentUserLastMessageSnapshot.exists) {
        const currentUserLastMessageData = currentUserLastMessageSnapshot.data();
        console.log('Current User Last Message:', currentUserLastMessageData);
      } else {
        console.log('No last message found for the current user.');
      }
    });

    // Listen for changes to the last message document for the other user
    const otherUserLastMessageRef = firestore.collection('conversations').doc(conversationID).collection('lastMessages').doc(otherUserId);
    const otherUserUnsubscribe = otherUserLastMessageRef.onSnapshot((otherUserLastMessageSnapshot) => {
      if (otherUserLastMessageSnapshot.exists) {
        const otherUserLastMessageData = otherUserLastMessageSnapshot.data();
        console.log('Other User Last Message:', otherUserLastMessageData);
      } else {
        console.log('No last message found for the other user.');
      }
    });

    // Return the unsubscribe functions to stop listening when necessary
    return [currentUserUnsubscribe, otherUserUnsubscribe];
  } catch (error) {
    console.error('Error fetching last message:', error);
  }
};


  const handleSendMessage = async () => {
  try {
    // Check if newMessage is empty or if no conversation is selected
    if (newMessage.trim() === '' || !selectedConversation) {
      console.error('Error: Empty message or no conversation selected.');
      return;
    }

    const user = auth.currentUser;

    // Fetch data from admins collection for the current user
    const userDataRef = firestore.collection('admins').doc(user.uid);
    const userDataSnapshot = await userDataRef.get();
    
    // Check if userData exists and has a displayName
    if (!userDataSnapshot.exists || !userDataSnapshot.get('displayName')) {
      console.error('Error: User data or displayName is undefined.');
      return;
    }

    // Retrieve user data fields
    const displayName = userDataSnapshot.get('displayName');
    const photoURL = userDataSnapshot.get('photoURL');
    const specialty = userDataSnapshot.get('specialty');

    const conversationRef = firestore.collection('conversations').doc(selectedConversation);
    const messageRef = conversationRef.collection('messages').doc();
    
    // Add the message to the conversation messages collection
    await messageRef.set({
      text: newMessage,
      timestamp: new Date(),
      displayName,
      photoURL,
      userUID: user.uid,
      specialty,
    });

    // Update the current user's last message in the lastMessages subcollection
    const lastMessageRef = conversationRef.collection('lastMessages').doc(user.uid);
    await lastMessageRef.set({
      text: newMessage,
      timestamp: new Date(),
      displayName,
      photoURL,
      userUID: user.uid,
      specialty,
    });

    // Clear the new message input field after sending
    setNewMessage('');
  } catch (error) {
    console.error('Error sending message: ', error);
  }
};

  const handleInputKeyDown = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      handleSendMessage();
    }
  };

  const handleEmojiSelect = (emoji) => {
    setNewMessage(newMessage + emoji);
  };

  const handleMessageClick = (message) => {
    setSelectedMessage(message);
  };

  const handleEmojiPickerToggle = () => {
    setShowEmojiPicker(!showEmojiPicker);
  };

  const handleBackButtonClick = () => {
    setSelectedConversation(null);
  };

  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
  };

  const filteredUsers = users.filter((user) =>
    user.displayName.toLowerCase().includes(searchTerm.toLowerCase())
  );

  return (
    <div className="chat-container">
      {selectedConversation ? (
        <>
          <button onClick={handleBackButtonClick} className="back-button">
            Back
          </button>
          <div className="message-list" ref={messageListRef}>
            {messages.map((message) => (
              <div
                key={message.id}
                className={`message ${message.userUID === auth.currentUser.uid ? 'sent' : 'received'}`}
                onClick={() => handleMessageClick(message)}
              >
                <div className="message-header">
                  <img src={message.photoURL || 'URL_TO_DEFAULT_AVATAR_IMAGE'} alt={`${message.displayName}'s profile`} className="message-avatar" />
                  <div className="message-sender">
                    {message.displayName}
                    <div className="specialty-bubble">
                      {message.specialty}
                    </div>
                  </div>
                </div>
                <div className="message-text">
                  {message.text}
                </div>
                {selectedMessage && selectedMessage.id === message.id && (
                  <div className="message-details">
                    {message.timestamp.toDate().toLocaleString()}
                  </div>
                )}
              </div>
            ))}
          </div>
          <div className="chat-input">
            <button onClick={handleEmojiPickerToggle} className="emoji-button">
              🙂
            </button>
            {showEmojiPicker && (
              <div className="emoji-picker">
                <div className="emoji-picker-inner">
                  {emojiList.map((emoji) => (
                    <span key={emoji} onClick={() => handleEmojiSelect(emoji)}>
                      {emoji}
                    </span>
                  ))}
                </div>
              </div>
            )}
            <input
              type="text"
              placeholder="Type a message"
              value={newMessage}
              onChange={(e) => setNewMessage(e.target.value)}
              onKeyDown={handleInputKeyDown}
            />
            <button onClick={handleSendMessage}>Send</button>
          </div>
        </>
      ) : (
        <div className="user-list">
          <input
            type="text"
            placeholder="Search for a user"
            value={searchTerm}
            onChange={handleSearch}
          />
          {filteredUsers.map((user) => (
            <div key={user.uid} onClick={() => handleUserClick(user.uid)}>
              <img src={user.photoURL || 'URL_TO_DEFAULT_AVATAR_IMAGE'} alt={`${user.displayName}'s profile`} className="user-avatar" />
              <div className="user-info">
                <div>{user.displayName}</div>
                <div className="specialty-bubble">
                  {user.specialty || 'Student'}
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default Chat;
